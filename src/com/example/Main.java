package com.example;

import java.util.Random;

public class Main {

    private static final int A = 50;
    private static final int B = 50;
    private static final char EMPTY = ' ';
    private static final char FULL = 'x';
    private static final int[][] FIELD = new int[A][B];

    public static void main(String[] args) {
        drawField(A, B);
    }
    private static void drawField(int a, int b){
        Random random = new Random();
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                FIELD[i][j] = random.nextInt(2);
                if (FIELD[i][j] == 0) {
                    System.out.print(EMPTY);
                }
                else System.out.print(FULL);
            }
            System.out.println();
        }
    }

}
